package com.electez.finalyear;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

public class Billpage extends AppCompatActivity{
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    Login login;
    String consumer = login.consumer;
    String name = login.name;
    double unit= login.unit ;
    String time = login.time;
    String month;
    String mobile = login.phoneText;
    String bill;
    int UPI_PAYMENT=123;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.billpage);
        billvalue();
    }

    public void billvalue() {
        TextView textView1,textView2,textView3,textView4,textView5,textView6;
        period();
        calculation();
        textView1 = findViewById(R.id.consnumber);
        textView2 = findViewById(R.id.mobnumber);
        textView3 = findViewById(R.id.nameid);
        textView4 = findViewById(R.id.unitid);
        textView5 = findViewById(R.id.monthid);
        textView6 = findViewById(R.id.payid);
        textView1.setText(consumer);
        textView2.setText(mobile);
        textView3.setText(name);
        textView4.setText(Double.toString(unit));
        textView5.setText(month);
        textView6.setText("Rs" + bill);
    }


        private void calculation() {
            double value = 0.0;
            if(unit<=50) value = (3.45 * unit);
            else if(unit<=100) value = (4.70 * unit);
            else if(unit<=150) value = (5.45 * unit);
            else if(unit<=200) value = (5.85 * unit);
            else if(unit<=250) value = (6.50 * unit);

            bill = (Double.toString(value));
        }


    private void period() {
        Calendar cal = Calendar.getInstance();
        month = cal.get(Calendar.MONTH) + "-" + cal.get(Calendar.YEAR) +
                " to " + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);

    }
    @SuppressLint("QueryPermissionsNeeded")
    public void payment(View view) {
        Uri uri = Uri.parse("upi://pay?pa=keralacmdrf@sbi&pn=Kerala%20Chief%20Ministers%20Distress%20Relief%20Fund&" +
                "mc=9399&tid=&tr=&tn=Kerala%20CM%20Distress%20Relief%20Fund%20for%20Floods&" +
                "am="+bill+"&mam=&cu=&url=https://www.cmdrf.kerala.gov.in&");
        Intent upiPayIntent = new Intent(Intent.ACTION_VIEW);
        upiPayIntent.setData(uri);

        // will always show a dialog to user to choose an app
        Intent chooser = Intent.createChooser(upiPayIntent, "Pay with");

        // check if intent resolves
        if(null != chooser.resolveActivity(getPackageManager())) {
            startActivityForResult(chooser, UPI_PAYMENT);
        } else {
            Toast.makeText(Billpage.this,"No UPI app found, please install one to continue",Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TextView textView;
        textView = findViewById(R.id.textView9);
        if (requestCode == UPI_PAYMENT) {
            if (resultCode == RESULT_OK) {
                assert data != null;
                firebaseDatabase = FirebaseDatabase.getInstance();
                databaseReference = firebaseDatabase.getReference("Consumer/");
                databaseReference.child(consumer).child(time).child("paid").setValue("Yes");
                Log.e("data", "response " + data.getStringExtra("response"));
                Toast.makeText(this, "response : " + data.getStringExtra("response"), Toast.LENGTH_LONG).show();
                textView.setText("Bill Paid");
            } else {
                Toast.makeText(this, "Not successful", Toast.LENGTH_LONG).show();
                textView.setText("Unsuccessful");
            }
        } else {
            Toast.makeText(this, "Not paid ", Toast.LENGTH_LONG).show();
            textView.setText("Successful");
        }
    }
}
