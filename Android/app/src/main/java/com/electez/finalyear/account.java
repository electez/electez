package com.electez.finalyear;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class account extends AppCompatActivity {
    Login login;
    String consumer=login.consumer;
    String name = login.name;
    String mobile = login.phoneText;
    Double limit = login.limit;
    TextView t1,t2,t3,t4;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account);
        t1 = findViewById(R.id.aconsnumber);
        t2 = findViewById(R.id.mobnumber);
        t3 = findViewById(R.id.nameid);
        t4 = findViewById(R.id.limits);
        t1.setText(consumer);
        t2.setText(mobile);
        t3.setText(name);
        t4.setText(Double.toString(limit));

    }
    public void accounttobill(View view){
        Intent intent = new Intent(account.this,home.class);
        startActivity(intent);
    }

}
