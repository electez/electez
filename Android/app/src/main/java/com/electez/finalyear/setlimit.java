package com.electez.finalyear;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class setlimit extends AppCompatActivity {
    String consumer = Login.consumer;
    double limit;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setlimit);
    }
    public void hundred(View view){
        limit=100;
        upload();
    }
    public void hundredfifty(View view){
        limit=150;
        upload();
    }
    public void twohundred(View view){
        limit=200;
        upload();
    }
    public void upload(){
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Consumer/");
        databaseReference.child(consumer).child("Warning").setValue(limit);
        Intent intent = new Intent(setlimit.this,account.class);
        startActivity(intent);
    }
    public void goback(View view){
        Intent intent = new Intent(setlimit.this,home.class);
        startActivity(intent);
    }
}
