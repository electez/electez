package com.electez.finalyear;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

public class Login extends AppCompatActivity{
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    public static String time,time1,consumer,name,phoneText,warn,paid;
    public static double unit,limit;
    TextView textview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

    }
    public void loginbutton(View view){
        textview = findViewById(R.id.checktext);
        EditText editText1 = findViewById(R.id.editconsnumber);
        EditText editText2 = findViewById(R.id.editmobile);
        consumer = editText1.getText().toString();
        phoneText = editText2.getText().toString();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Consumer/");
        date();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                boolean testing = snapshot.child(consumer).exists();
                String x;
                if(testing==true){
                    String mobile = snapshot.child(consumer).child("Mobile").getValue(String.class);
                    boolean y = phoneText.equals(mobile);
                    if(y==true){
                        values(snapshot);
                        Intent intent = new Intent(Login.this,home.class);
                        startActivity(intent);
                    }
                    else{
                        x = "Please enter correct Mobile Number";
                    }
                }
                else{
                    x="Please enter correct Consumer Address";
                }
            }

            public void values(DataSnapshot snapshot) {
                name = snapshot.child(consumer).child("Name").getValue(String.class);
                double unit1 = snapshot.child(consumer).child(time).child("myVal").getValue(double.class);
                double unit2 = snapshot.child(consumer).child((time1)).child("myVal").getValue(double.class);
                unit = unit1 - unit2;
                limit = snapshot.child(consumer).child("Warning").getValue(double.class);
                warn = snapshot.child(consumer).child("warn").getValue(String.class);
                paid = snapshot.child(consumer).child(time).child("paid").getValue(String.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                textview.setText("Please Check Your Internet Connection");
            }
        });

    }

    public void date() {
        Calendar cal = Calendar.getInstance();
        time = String.valueOf((cal.get(Calendar.YEAR)*10)+ (cal.get(Calendar.MONTH) + 1));
        time1 = String.valueOf((cal.get(Calendar.YEAR)*10)+ cal.get(Calendar.MONTH));
    }

}
