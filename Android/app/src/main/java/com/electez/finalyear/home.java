package com.electez.finalyear;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class home extends AppCompatActivity {
    Login login;
    String warn = login.warn;
    String limit = Double.toString(login.limit);
    String msg = Double.toString(Login.limit);
    String paid = login.paid;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        textView = findViewById(R.id.warning);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("Electez","Electez", NotificationManager.IMPORTANCE_DEFAULT);
//            NotificationChannel channels = new NotificationChannel("Paid","Bill Paid", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            NotificationManager managers = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
//            managers.createNotificationChannel(channels);
//            NotificationChannel channel1 = new NotificationChannel("Paid","Bill Paid", NotificationManager.IMPORTANCE_DEFAULT);
//            NotificationManager manager1 = getSystemService(NotificationManager.class);
//            manager.createNotificationChannel(channel1);
        }
        String no ="No";
        boolean ch = paid.equals(no);
        if(ch==true){
            bill_function();
        }
        String yes = "Yes";
        boolean x = warn.equals(yes);
        if(x==true){
            notification_function();
        }
    }

    private void bill_function() {
        String textTitle="Bill Not Paid";
        String textContent="Please pay your bill";
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"Electez")
                .setSmallIcon(R.drawable.ic_notificationmessage)
                .setContentTitle(textTitle)
                .setContentText(textContent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH);
//                .setAutoCancel(true);

        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(home.this);
        managerCompat.notify(2,builder.build());
    }

    private void notification_function() {
        String textTitle="Limit Exceeded";
        String textContent=limit + " units consumed";
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"Electez")
                .setSmallIcon(R.drawable.ic_notificationmessage)
                .setContentTitle(textTitle)
                .setContentText(textContent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH);
//                .setAutoCancel(true);

        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(home.this);
        managerCompat.notify(1,builder.build());
    }

    public void accountpage(View view){
        Intent intent = new Intent(home.this,account.class);
        startActivity(intent);
    }
    public void setlimit(View view){
        Intent intent = new Intent(home.this,setlimit.class);
        startActivity(intent);
    }

    public void aboutus(View view){
        Intent intent = new Intent(home.this,About.class);
        startActivity(intent);
    }
    public void billit(View view){
        Intent intent = new Intent(home.this,Billpage.class);
        startActivity(intent);

    }
    public void logout(View view){
        Intent intent = new Intent(home.this,Login.class);
        startActivity(intent);
    }


    }
