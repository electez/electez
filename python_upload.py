from typing import Dict
import firebase_admin
from firebase_admin import db
import datetime
import random
import json
import time

from paho.mqtt import client as mqtt_client


broker = 'xxxxxxxx'
port = 1883
topic = "xxxxxxx"
topics = 'xxxxxxxxx'
# generate client ID with pub prefix randomly
client_id = f'xxxxxxx'
username = 'xxxxxxxxxxxxxx'
password = 'xxxxxxxxxxxx'
x= datetime.date.today()
y = (x.year*10)+x.month

# deveui=""
# h=0


def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def firebase(deveui,h):
    print("Connecting to Electez Firebase")
    if(deveui == "xxxxxxxxxx"):
        Consumer = 9876
        Mobile = 9876543210
    path = "/Consumer/"+str(Consumer)+"/"
    ref = db.reference(path)
    limit = ref.child("Warning").get()
    previous = ref.child(str(y-1)).child("myVal").get()
    check = h-previous
    if(x.day==18):
        value="No"
        z={"myVal":h,"paid":"No"}
        userinfo={"Mobile":str(Mobile),"Name":"Test","warn":value,y:z}
        ref.update(userinfo)
        print("Unit = "+str(h))

    elif(check>limit):
        value="Yes"
        valdic={"warn":value}
        ref.update(valdic)        


def subscribe(client: mqtt_client):
    print("Subscribed to topic")
    def on_message(client, userdata, msg):
        print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
        print(msg.payload.decode())
        obj = json.loads(msg.payload)
        print(obj)
        deveui = obj["deveui"]
        i=obj["payload"]
        print(i)
        h=(i["myVal"])
        firebase(deveui,h)
        print("Checking if bill is paid")
        path = "/Consumer/"+str(9876)+"/"
        ref = db.reference(path)
        check = ref.child(str(y-1)).child("paid").get()
        if(check=="Yes"):
            print("Bill of "+ str((x.month-1))+"-"+str(x.year)+" paid")
        else:
            print("Bill of "+ str(x.month-1)+"-"+str(x.year)+" not paid")
            print("Sending message to cut connection")
            msg = '2'
            payload = json.dumps({ "appid": 98,"deviceeui":"xxxxxxxxxxx","data":msg })
            result = client.publish(topics, payload)
            print(payload)
            status = result[0]
            if status == 0:
                print(f"Send `{msg}` to topic `{topics}`")
            else:
                print(f"Failed to send message to topic {topics}")
    
    client.subscribe(topic)
    client.on_message = on_message
    

def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()


if __name__ == '__main__':
    cred_obj = firebase_admin.credentials.Certificate('xxxxxxxxxxxxxxx')
    default_app = firebase_admin.initialize_app(cred_obj, {
	'databaseURL':"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"})
    run()


